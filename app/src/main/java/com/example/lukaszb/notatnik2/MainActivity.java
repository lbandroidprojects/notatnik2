package com.example.lukaszb.notatnik2;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    public static final int NOTATKA_REQUEST_CODE=11;
    private Button bNotatka;

    private ArrayList<String> notatki;
    private ListView lvNotatki;
    private ArrayAdapter<String> adapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        bNotatka=findViewById(R.id.bNotatka);
        bNotatka.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i=new Intent(view.getContext(),SecondActivity.class);
                startActivityForResult(i,NOTATKA_REQUEST_CODE);
            }
        });

        if(savedInstanceState==null)
          notatki=new ArrayList<>();
        else
          notatki=savedInstanceState.getStringArrayList("lista");
        lvNotatki=findViewById(R.id.lvNotatki);
        adapter=new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1,notatki);
        lvNotatki.setAdapter(adapter);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode==NOTATKA_REQUEST_CODE && resultCode==RESULT_OK){
            String text=data.getStringExtra("notatka");
            notatki.add(text);
            adapter.notifyDataSetChanged();
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putStringArrayList("lista",notatki);
    }

//    @Override
//    protected void onRestoreInstanceState(Bundle savedInstanceState) {
//        super.onRestoreInstanceState(savedInstanceState);
//        if(savedInstanceState!=null)
//            notatki=savedInstanceState.getStringArrayList("lista");
//    }
}
