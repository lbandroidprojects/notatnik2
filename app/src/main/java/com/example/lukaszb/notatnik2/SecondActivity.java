package com.example.lukaszb.notatnik2;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class SecondActivity extends AppCompatActivity implements View.OnClickListener{
    private Button bOK,bAnuluj;
    private EditText etNotatka;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);

        bOK=findViewById(R.id.bOK);
        bAnuluj=findViewById(R.id.bAnuluj);
        etNotatka=findViewById(R.id.etNotatka);
        bOK.setOnClickListener(this);
        bAnuluj.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch(view.getId()){
            case R.id.bOK:
                if(etNotatka.getText().length()>0)
                {
                    Intent i=new Intent();
                    i.putExtra("notatka",etNotatka.getText().toString());
                    setResult(RESULT_OK,i);
                    finish();
                }
                break;
            case R.id.bAnuluj:
                setResult(RESULT_CANCELED);
                finish();
                break;
        }
    }
}
